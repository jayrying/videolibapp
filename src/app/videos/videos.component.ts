import { VideoApiService } from './../shared/services/video-api-service/video-api.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VideoCategory } from './../shared/models/video-category-model/video-category';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html'
})
export class VideosComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private videoApiService: VideoApiService,
    private router: Router
  ) {}
  data: VideoCategory = {
    videoCategoryId: 0,
    name: '',
    videos: [],
    img: ''
  };
  search = '';

  ngOnInit(): void {

    this.videoApiService.selectedCategory$.subscribe((res: VideoCategory) => {
      this.data = res;
    });
    this.route.params.subscribe((params: any) => {
      this.videoApiService.setSelectedCategory(params.id || 0);
    });

  }

  onBack() {
    this.router.navigateByUrl('/categories');
  }
}
