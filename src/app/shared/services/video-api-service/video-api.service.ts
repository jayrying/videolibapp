import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import dataFeeds from './../datafeed.json';

@Injectable({
  providedIn: 'root'
})
export class VideoApiService {

  private categoriesValue = dataFeeds.categories;
  private categories = new BehaviorSubject(this.categoriesValue);
  categories$: Observable<object[]> = this.categories.asObservable();


  private selectedCategoryValue = {};
  private selectedCategory = new BehaviorSubject(this.selectedCategoryValue);
  selectedCategory$: Observable<object> = this.selectedCategory.asObservable();

  constructor() { }

  setSelectedCategory(id) {
    this.selectedCategory.next(
      this.categoriesValue.find(v => v.videoCategoryId == id)
    );
  }

}
