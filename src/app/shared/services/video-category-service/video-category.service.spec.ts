import { TestBed } from '@angular/core/testing';

import { VideoCategoryService } from './video-category.service';

describe('VideoCategoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VideoCategoryService = TestBed.get(VideoCategoryService);
    expect(service).toBeTruthy();
  });
});
