import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchFilter'
})
export class SearchFilterPipe implements PipeTransform {

  transform(value: any, searchTerm?: any, searchField?: any): any {
    if (searchTerm) {
      return value.filter((e: any) => {
        return e[searchField].toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
      });
    } else {
      return value;
    }
  }

}
