export class Video {
  videoId: number;
  videoCategoryId: number;
  img: string;
  title: string;
  videoUrl: string;

  tags: string[];
  description: string;
  durMin: number;
}
