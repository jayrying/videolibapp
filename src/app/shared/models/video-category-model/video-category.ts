import { Video } from 'src/app/shared/models/video-model/video';

export class VideoCategory {
  videoCategoryId: number;
  name: string;
  img: string;
  videos: Video[]
}
