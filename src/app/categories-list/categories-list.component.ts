import { Component, OnInit } from '@angular/core';
import { VideoApiService } from './../shared/services/video-api-service/video-api.service';
import { VideoCategory } from '../shared/models/video-category-model/video-category';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html'
})
export class CategoriesListComponent implements OnInit {
  // tslint:disable-next-line:no-any
  categories: VideoCategory[] = [];
  search = '';

  constructor(
    private videoApiService: VideoApiService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.videoApiService.categories$.subscribe((res: []) => {
      this.categories = res;
    });
  }

  exploreCategory(item) {
    console.log('item', item);
  }

  onBack() {
    this.router.navigateByUrl('/home');
  }


}
