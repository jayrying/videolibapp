import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  @Input() isCollapsed;
  @Input() username;
  url = '';

  constructor(private router: Router) { }

  ngOnInit() {
  }

  routeIsActive(routePath: string) {
    return this.router.url === routePath;
  }

}
